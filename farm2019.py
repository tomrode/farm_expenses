# Name:      farm2019.py
#
# Purpose:  This python script outputs expenses, incomes from the farm  
#
# Author:    Thomas Rode
#
# Options:  None
#
# Note:
#
# Python Version: 3.7.4
#          
# Reference: https://www.python.org/ (open source)
#


import csv
import os


#############################################################################################################
#                                        EXPENSES
#  Note: All numerical entries here. If you need to add or replace expense grouping i.e "countyexpensedictx"
#        please search for this dictonary variable in script accordingly
# 
#############################################################################################################
countyexpensedictx = [
    {'Date': '1/28/2019','Income/Expense': 'Expense','Description':'putnamCountyTreasure', 'Amount'  : 1265.18},
    {'Date': '1/19/2019','Income/Expense': 'Expense','Description':'allenCountyTreasure',  'Amount'  :  646.94},
    ]
propaneexpensedictx = [
    {'Date': '1/23/2019','Income/Expense': 'Expense','Description':'fortJenningsPropane1',  'Amount' :  226.19},
    {'Date': '2/21/2019','Income/Expense': 'Expense','Description':'fortJenningsPropane2',  'Amount' :  373.32},
    {'Date': '3/24/2019','Income/Expense': 'Expense','Description':'fortJenningsPropane3',  'Amount' :  499.76},
    {'Date': '12/3/2019','Income/Expense': 'Expense','Description':'fortJenningsPropane4',  'Amount' :  356.38},
    ]
cauvexpensedictx = [
    {'Date': '2/20/2019','Income/Expense': 'Expense','Description':'cauvputnam',          'Amount' :  25.00},
    {'Date': '2/21/2019','Income/Expense': 'Expense','Description':'cauvallen',          'Amount' :  25.00},
    ]
germanmutualexpensedictx = [
    {'Date': '2/7/2019','Income/Expense': 'Expense','Description':'germanmutualfirst',  'Amount' :  671.55},
    {'Date': '7/24/2019','Income/Expense': 'Expense','Description':'germanmutualsecond',  'Amount' :  668.55},
    ]
miscexpensesdictx = [
    {'Date': '2/12/2019','Income/Expense': 'Expense','Description':'stevemeyerservice',    'Amount' :  424.88},
    {'Date': '2/13/2019','Income/Expense': 'Expense','Description':'aep',                  'Amount' :  199.08},
    {'Date': '2/21/2019','Income/Expense': 'Expense','Description':'citipayoff',          'Amount' :  501.29},
    {'Date': '4/19/2019','Income/Expense': 'Expense','Description':'b&ktruckingcamperrock','Amount' :  145.77},
    {'Date': '5/20/2019','Income/Expense': 'Expense','Description':'b&ktruckingpebblerock','Amount' :  222.04},
    {'Date': '5/20/2019','Income/Expense': 'Expense','Description':'tom&allison',          'Amount' :  200.00},
    {'Date': '5/29/2019','Income/Expense': 'Expense','Description':'tom&allison2',        'Amount' :  300.00},
    ]
fieldexpensesdictx = [
    {'Date': '5/6/2019','Income/Expense': 'Expense','Description':'wheatspray20acres',          'Amount' :  513.80},
    {'Date': '6/10/2019','Income/Expense': 'Expense','Description':'wheatdust20acres&soy10acres', 'Amount' :  453.70},
    {'Date': '7/17/2019','Income/Expense': 'Expense','Description':'soybeanseed10acres',          'Amount' :  552.69},
    {'Date': '8/10/2019','Income/Expense': 'Expense','Description':'soybeanspray10acres',        'Amount' :  172.72},
    {'Date': '11/12/2019','Income/Expense': 'Expense','Description':'wheatspray20acres',          'Amount' :  441.04},
    ]

limeapplicationexpensedictx = [
    {'Date': '10/10/2019','Income/Expense': 'Expense','Description':'limeforsoil',                  'Amount' : 1937.16},
    ]

year2020expensedictx = [
    {'Date': '9/6/2019','Income/Expense': 'Expense','Description':'wheatseed10acresforyear2020',  'Amount' : 320.00}, 
    {'Date': '12/10/2019','Income/Expense': 'Expense','Description':'cornspray20acresforyear2020',  'Amount' : 951.22}, 
    ]

#############################################################################################################
#                                        INCOME
#  Note: All numerical entries here. If you need to add or replace expense grouping i.e "fieldincomedictx"
#        please search for this dictonary variable in script accordingly
# 
#############################################################################################################
fieldincomedictx = [
    {'Date': '7/20/2019','Income/Expense': 'Income','Description':'wheat20acres',                  'Amount' :4395.19},
    {'Date': '7/29/2019','Income/Expense': 'Income','Description':'wheatstraw20acres',            'Amount' :1146.60}, 
    {'Date': '10/19/2019','Income/Expense': 'Income','Description':'soybeanseed10acres',            'Amount' :2863.68},
    ]

fsaincomedictx = [
    {'Date': '10/22/2019','Income/Expense': 'Income','Description':'fsaincomeoctober',              'Amount' :421.20},
    {'Date': '11/20/2019','Income/Expense': 'Income','Description':'fsaincomenovember',            'Amount' :221.60},
    ]
########################################## END ################################################################

def WriteDictToCSV(csv_file,csv_columns,dict_data):
    try:
        with open(csv_file, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in dict_data:
                writer.writerow(data)
    except: #IOError as (errno, strerror):
            print("Error")#("I/O error({0}): {1}".format(errno, strerror))    
    return 
    
def expensecalc():
    
    #sum up individual dictonary's

    # Column headers
    csv_columns                      = ['Date','Income/Expense','Description', 'Amount']
    csv_column_spacer                = ['','','']
    csv_column_expense_total          = ['','','Expenses Total']
    csv_column_income_total          = ['','','Income Total']
    csv_column_grand_total            = ['','','Grand Total']
    csv_column_grouped_expense_income = ['Income/Expense', 'Grouping Description','Total']

    # adding up expenses and adding to .csv
    countyexpensedictxval          = sum(item['Amount'] for item in countyexpensedictx)    
    propaneexpensedictxval        = sum(item['Amount'] for item in propaneexpensedictx)
    cauvexpensedictxval            = sum(item['Amount'] for item in cauvexpensedictx)
    germanmutualexpensedictxval    = sum(item['Amount'] for item in germanmutualexpensedictx)
    miscexpensesdictxval          = sum(item['Amount'] for item in miscexpensesdictx)
    fieldexpensesdictxval          = sum(item['Amount'] for item in fieldexpensesdictx)
    limeapplicationexpensedictxval = sum(item['Amount'] for item in limeapplicationexpensedictx)
    year2020expensedictxval        = sum(item['Amount'] for item in year2020expensedictx)

    # cumulative expenses total
    csv_column_expense_total.append(countyexpensedictxval + propaneexpensedictxval + cauvexpensedictxval + germanmutualexpensedictxval + miscexpensesdictxval + fieldexpensesdictxval + limeapplicationexpensedictxval + year2020expensedictxval)
    allexpenses = (countyexpensedictxval + propaneexpensedictxval + cauvexpensedictxval + germanmutualexpensedictxval + miscexpensesdictxval + fieldexpensesdictxval + limeapplicationexpensedictxval + year2020expensedictxval)

    # adding up income and adding to .csv
    fieldincomedictxval = sum(item['Amount'] for item in fieldincomedictx)
    fsaincomedictxval = sum(item['Amount'] for item in fsaincomedictx)

    # cumulative income total
    csv_column_income_total.append(fieldincomedictxval + fsaincomedictxval)
    allincomes = (fieldincomedictxval + fsaincomedictxval)

    # Overall total income - expenses
    csv_column_grand_total.append(allincomes - allexpenses)

    # print out results
    print("##############  EXPENSES ################")
    print("countyexpensedict sum ->        ", round(countyexpensedictxval, 2))
    print("propaneexpensedict sum ->        ", propaneexpensedictxval)
    print("cauvexpensedict sum ->          ", cauvexpensedictxval)
    print("germanmutualexpensedict sum ->  ", germanmutualexpensedictxval)
    print("miscexpensesdict sum ->          ", miscexpensesdictxval)
    print("fieldexpensesdict sum ->        ", round(fieldexpensesdictxval, 2))
    print("limeapplicationexpensedict sum ->", limeapplicationexpensedictxval)
    print("year2020expensedict sum ->      ", year2020expensedictxval)
    print("")
    print("Expense Total in dollars        $",round(allexpenses,2))
    print("")
    print("##############  INCOME ##################")
    print("fieldincomedict sum ->          ", fieldincomedictxval)
    print("fsaincomedict sum ->            ", fsaincomedictxval)
    print("")
    print("Income Total in dollars        $",round(allincomes,2))
    print("")
    print("Net result in dollars          $",round(allincomes - allexpenses,2))
    print("")
    print("##############  ITEMS ##################")
    print("")
    """
    print(countyexpensedictx)
    print(propaneexpensedictx)
    print(cauvexpensedictx)
    print(germanmutualexpensedictx)
    print(miscexpensesdictx)
    print(fieldexpensesdictx)
    print(limeapplicationexpensedictx)
    print(year2020expensedictx)
    print(fieldincomedictx)
    print(fsaincomedictx)
    """
    # setting up .csv
    currentPath = os.getcwd()
    csv_file = "farm2019.csv"

    # expense dictionary entries
    try:
        with open(csv_file, 'w', newline='') as csvfile:
            #Expenses
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in countyexpensedictx:
                writer.writerow(data)
            for data in propaneexpensedictx:
                writer.writerow(data)
            for data in cauvexpensedictx:
                writer.writerow(data)
            for data in germanmutualexpensedictx:
                writer.writerow(data)
            for data in miscexpensesdictx:
                writer.writerow(data)
            for data in fieldexpensesdictx:
                writer.writerow(data)
            for data in limeapplicationexpensedictx:
                writer.writerow(data)
            for data in year2020expensedictx:
                writer.writerow(data)        
            #write the total for this expense
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_expense_total)
            writer.writeheader()
            
            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()

            #Incomes
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            #writer.writeheader()
            for data in fieldincomedictx:
                writer.writerow(data)
            for data in fsaincomedictx:
                writer.writerow(data)
            #write the total for this incomes
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_income_total)
            writer.writeheader()

            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()

            #The overall total
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_grand_total)
            writer.writeheader()

            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()
            writer.writerow({'':''}) # nothing

            #place totals from income expense goupings
            writer = csv.DictWriter(csvfile,csv_column_grouped_expense_income)
            writer.writeheader()
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Property taxes Expenses' ,'Total': countyexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Propane Expenses' ,'Total': propaneexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Cauv Expenses' ,'Total': cauvexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total German Mutual Insurance' ,'Total': germanmutualexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Miscellaneous Expenses' ,'Total':  miscexpensesdictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Field Expenses' ,'Total':  fieldexpensesdictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Field 5 Year Lime Expenses' ,'Total':  limeapplicationexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Year 2020 Field Expenses' ,'Total':  year2020expensedictxval})
    except:
        print("Error opening .csv file")
    
# Main program - Supports command line arguments if needed "
if __name__ == "__main__":

    # Call calculations
    expensecalc()
