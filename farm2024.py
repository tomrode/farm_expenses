# Name:      farm2024.py
#
# Purpose:  This python script outputs expenses, incomes from the farm  
#
# Author:    Thomas Rode
#
# Options:  None
#
# Note:
#
# Python Version: 3.7.4
#          
# Reference: https://www.python.org/ (open source)
#


import csv
import os

csvFileName = "farm2024.csv"

#############################################################################################################
#                                        EXPENSES
#  Note: All numerical entries here. If you need to add or replace expense grouping i.e "countyexpensedictx"
#        please search for this dictonary variable in script accordingly
# 
#############################################################################################################
countyexpensedictx = [
    {'Date': '1/20/2024','Income/Expense': 'Expense','Description':'putnamCountyTreasure', 'Amount'  : 1929.03},
    {'Date': '7/10/2024','Income/Expense': 'Expense','Description':'putnamCountyTreasure paid from main account', 'Amount'  : 1916.13},
    ]
propaneexpensedictx = [
    {'Date': '1/24/2024','Income/Expense': 'Expense','Description':'fortJenningsPropane1',  'Amount' : 409.94},
    {'Date': '3/02/2024','Income/Expense': 'Expense','Description':'fortJenningsPropane2 ', 'Amount' : 547.45},
    {'Date': '7/19/2024','Income/Expense': 'Expense','Description':'fortJenningsPropane3',  'Amount' : 219.09},
    ]
cauvexpensedictx = [
    {'Date': '1/1/2024','Income/Expense': 'Expense','Description':'cauvputnam',          'Amount' :  0.00},
    ]
germanmutualexpensedictx = [
    {'Date': '1/20/2024','Income/Expense': 'Expense','Description':'germanmutualfirst',  'Amount' :  981.50},
    {'Date': '7/31/2024','Income/Expense': 'Expense','Description':'germanmutualsecond',  'Amount' :  981.50},
    ]
miscexpensesdictx = [
    {'Date': '1/02/2024','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 40.00},
    {'Date': '1/16/2024','Income/Expense': 'Expense','Description':'Henrys Car Purchase',    'Amount' : 3000.00},
    {'Date': '1/18/2024','Income/Expense': 'Expense','Description':'AEP Ohio', 'Amount' : 389.80},
    {'Date': '2/23/2024','Income/Expense': 'Expense','Description':'County Line Tow Henrys car',  'Amount' : 125.00},
    {'Date': '3/15/2024','Income/Expense': 'Expense','Description':'AEP Ohio',    'Amount' : 314.54},
    {'Date': '3/25/2024','Income/Expense': 'Expense','Description':'Ohio SD Tax',    'Amount' : 972.00},
    {'Date': '4/19/2024','Income/Expense': 'Expense','Description':'DCC-HenryJR MBR',    'Amount' : 199.00},
    {'Date': '4/25/2024','Income/Expense': 'Expense','Description':'Tom-Allison Cash',    'Amount' : 100.00},
    {'Date': '4/25/2024','Income/Expense': 'Expense','Description':'AEP Ohio',    'Amount' : 346.87},
    {'Date': '5/01/2024','Income/Expense': 'Expense','Description':'Cash Withraw Lavish',    'Amount' : 200.00},
    {'Date': '5/06/2024','Income/Expense': 'Expense','Description':'Tom-Allison Cash',    'Amount' : 130.00},
    {'Date': '5/21/2024','Income/Expense': 'Expense','Description':'Tom-Allison Cash',    'Amount' : 400.00},
    {'Date': '7/30/2024','Income/Expense': 'Expense','Description':'Tom-Allison Cash',    'Amount' : 620.00},
    {'Date': '11/05/2024','Income/Expense': 'Expense','Description':'Cash Withraw Lavish',    'Amount' : 105.00},
    {'Date': '11/09/2024','Income/Expense': 'Expense','Description':'Tree Trimming Dan',    'Amount' : 90.00},
    {'Date': '12/03/2024','Income/Expense': 'Expense','Description':'Cash Withraw Lavish',    'Amount' : 205.00},
    {'Date': '11/19/2024','Income/Expense': 'Expense','Description':'Tom-Allison Cash',    'Amount' : 250.00},
    {'Date': '11/20/2024','Income/Expense': 'Expense','Description':'Tom-Allison Cash',    'Amount' : 250.00},
    {'Date': '12/12/2024','Income/Expense': 'Expense','Description':'Tom-Allison Cash',    'Amount' : 250.00},
    ]
fieldexpensesdictx = [
    {'Date': '6/13/2024','Income/Expense': 'Expense','Description':'PopCorn spray10Acres invoice 54146417 ', 'Amount' : 433.04},
    {'Date': '6/13/2024','Income/Expense': 'Expense','Description':'Soybean spray20Acres invoice 54115496', 'Amount' :  546.60},
    {'Date': '6/13/2024','Income/Expense': 'Expense','Description':'PopCorn spray10Acres invoice 54500836',  'Amount' :  250.08},
    {'Date': '6/13/2024','Income/Expense': 'Expense','Description':'PopCorn spray10Acres invoice 54112089',  'Amount' :  227.96},
    {'Date': '6/14/2024','Income/Expense': 'Expense','Description':'PopCorn seed 10 acres',                 'Amount' :  533.20},
    {'Date': '8/13/2024','Income/Expense': 'Expense','Description':'Soybean spray20Acres',                  'Amount' :  488.49},
    {'Date': '12/13/2024','Income/Expense': 'Expense','Description':'Griding',                              'Amount' :  49.10},
    ]

limeapplicationexpensedictx = [
    {'Date': '1/1/2022','Income/Expense': 'Expense','Description':'limeforsoil',                  'Amount' : 0},
    ]

year2025expensedictx = [
    {'Date': '10/13/2024','Income/Expense': 'Expense','Description':'wheat seed for 20 acres for 2025 season',  'Amount' : 775.50},
    {'Date': '11/13/2024','Income/Expense': 'Expense','Description':'wheat spray/fertlizer for 20 acres for 2025 season',  'Amount' : 1588.90},
    {'Date': '12/31/2024','Income/Expense': 'Expense','Description':'soybean seed  for 10 acres for 2025 season',  'Amount' : 546.20},
    ]
 
#############################################################################################################
#                                        INCOME
#  Note: All numerical entries here. If you need to add or replace expense grouping i.e "fieldincomedictx"
#        please search for this dictonary variable in script accordingly
# 
#############################################################################################################
fieldincomedictx = [
    {'Date': '10/04/2024','Income/Expense': 'Income','Description':'Soybeans 20acres, (sold 603 Bu) @$9.72 Bu/Ac Bunge grain ', 'Amount' :5836.68},
    {'Date': '12/31/2024','Income/Expense': 'Income','Description':'PopCorn 10 acres, 4583Lbs/Ac, $370/Ton ', 'Amount' :4323.69},
    ]

fsaincomedictx = [
    {'Date': '1/1/2021','Income/Expense': 'Income','Description':'fsaincom cfap2','Amount' :0},
    ]

miscincomedictx = [
    {'Date': '6/27/2024','Income/Expense': 'Income','Description':'From Regular Account','Amount' :1000.00},
    {'Date': '7/31/2024','Income/Expense': 'Income','Description':'Payback From Henry','Amount'    :200.00},
    {'Date': '8/14/2024','Income/Expense': 'Income','Description':'Payback From Henry','Amount'    :500.00},
    {'Date': '12/13/2024','Income/Expense': 'Income','Description':'Carol birthday check','Amount'  :50.00},
    {'Date': '12/30/2024','Income/Expense': 'Income','Description':'Sale of Silverwear','Amount'   :800.00},
    ]
########################################## END ################################################################

def WriteDictToCSV(csv_file,csv_columns,dict_data):
    try:
        with open(csv_file, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in dict_data:
                writer.writerow(data)
    except: #IOError as (errno, strerror):
            print("Error")#("I/O error({0}): {1}".format(errno, strerror))    
    return 
    
def expensecalc():
    
    #sum up individual dictonary's

    # Column headers
    csv_columns                      = ['Date','Income/Expense','Description', 'Amount']
    csv_column_spacer                = ['','','']
    csv_column_expense_total          = ['','','All Expenses Total']
    csv_column_income_total          = ['','','All Income Total']
    csv_column_grand_total            = ['','','All Grand Total']
    csv_column_grouped_expense_income = ['Income/Expense', 'Grouping Description','Total']
    # Farm Only headers
    csv_columns_farm_only             = ['********************','FARM ONLY INCOME, EXPENSES, TOTALS ','********************']
    csv_column_expense_total_farm_only = ['','Farm Expenses Total']
    csv_column_income_total_farm_only =  ['','Farm Income Total']
    csv_column_grand_total_farm_only  =  ['','Farm Grand Total']
    

    # adding up expenses and adding to .csv
    countyexpensedictxval          = sum(item['Amount'] for item in countyexpensedictx)    
    propaneexpensedictxval        = sum(item['Amount'] for item in propaneexpensedictx)
    cauvexpensedictxval            = sum(item['Amount'] for item in cauvexpensedictx)
    germanmutualexpensedictxval    = sum(item['Amount'] for item in germanmutualexpensedictx)
    miscexpensesdictxval          = sum(item['Amount'] for item in miscexpensesdictx)
    fieldexpensesdictxval          = sum(item['Amount'] for item in fieldexpensesdictx)
    limeapplicationexpensedictxval = sum(item['Amount'] for item in limeapplicationexpensedictx)
    year2025expensedictxval        = sum(item['Amount'] for item in year2025expensedictx)

    # cumulative expenses total
    csv_column_expense_total.append(countyexpensedictxval + propaneexpensedictxval + cauvexpensedictxval + germanmutualexpensedictxval + miscexpensesdictxval + fieldexpensesdictxval + limeapplicationexpensedictxval + year2025expensedictxval)
    allexpenses                  = (countyexpensedictxval + propaneexpensedictxval + cauvexpensedictxval + germanmutualexpensedictxval + miscexpensesdictxval + fieldexpensesdictxval + limeapplicationexpensedictxval + year2025expensedictxval)

    # Farm only expenses
    csv_column_expense_total_farm_only.append(fieldexpensesdictxval + limeapplicationexpensedictxval + year2025expensedictxval)
    allfarmonlyexpenses = fieldexpensesdictxval + limeapplicationexpensedictxval + year2025expensedictxval

    # adding up income and adding to .csv
    fieldincomedictxval = sum(item['Amount'] for item in fieldincomedictx)
    fsaincomedictxval   = sum(item['Amount'] for item in fsaincomedictx)
    miscincomedictxval  = sum(item['Amount'] for item in miscincomedictx)

    # cumulative income total
    csv_column_income_total.append(fieldincomedictxval + fsaincomedictxval + miscincomedictxval)
    allincomes = (fieldincomedictxval + fsaincomedictxval + miscincomedictxval)
    
    # cumulative farm incomes only
    csv_column_income_total_farm_only.append(fieldincomedictxval + fsaincomedictxval)
    allfarmonlyincomes = fieldincomedictxval + fsaincomedictxval
    
    # Overall total income - expenses
    csv_column_grand_total.append(allincomes - allexpenses)
    
    # Overall farm income - expenses
    csv_column_grand_total_farm_only.append(allfarmonlyincomes - allfarmonlyexpenses)

    # print out results
    print("############## ALL EXPENSES ################")
    print("countyexpensedict sum ->        ", round(countyexpensedictxval, 2))
    print("propaneexpensedict sum ->        ", round(propaneexpensedictxval,2))
    print("cauvexpensedict sum ->          ", round(cauvexpensedictxval,2))
    print("germanmutualexpensedict sum ->  ", round(germanmutualexpensedictxval,2))
    print("miscexpensesdict sum ->          ", round(miscexpensesdictxval,2))
    print("fieldexpensesdict sum ->        ", round(fieldexpensesdictxval, 2))
    print("limeapplicationexpensedict sum ->", round(limeapplicationexpensedictxval,2))
    print("year2025expensedict sum ->      ", round(year2025expensedictxval, 2))
    print("")
    print("Expense Total in dollars        $",round(allexpenses,2))
    print("")
    print("############## ALL INCOME ##################")
    print("fieldincomedict sum ->          ", round(fieldincomedictxval ,2))
    print("fsaincomedict sum ->            ", round(fsaincomedictxval,2))
    print("miscincomedict sum ->            ", round(miscincomedictxval, 2))
    print("")
    print("Income Total in dollars        $",round(allincomes,2))
    print("")
    print("############## ALL INCOME MINUS EXPENSES ####")
    print("Net result in dollars          $",round(allincomes - allexpenses,2))
    print("\n\n")
    print("###################################################")
    print("##############  JUST FARM EXPENSES ################")
    print("###################################################")
    print("")
    print("fieldexpensesdict sum ->        ", round(fieldexpensesdictxval, 2))
    print("limeapplicationexpensedict sum ->", round(limeapplicationexpensedictxval,2))
    print("year2025expensedict sum ->      ", round(year2025expensedictxval, 2))
    print("")
    print("Farm Expense Total in dollars        $",round(allfarmonlyexpenses,2))
    print("##############  JUST FARM INCOME ################")
    print("")
    print("fieldincomedict sum ->          ", round(fieldincomedictxval ,2))
    print("fsaincomedict sum ->            ", round(fsaincomedictxval,2))
    print("")
    print("Farm  Total income in dollars        $",round(allfarmonlyincomes,2))
    print("############## ALL INCOME MINUS EXPENSES ####")
    print("Net result in dollars          $",round(allfarmonlyincomes - allfarmonlyexpenses,2))
    
    
    """
    print("##############  ITEMS ##################")
    print("")
    print(countyexpensedictx)
    print(propaneexpensedictx)
    print(cauvexpensedictx)
    print(germanmutualexpensedictx)
    print(miscexpensesdictx)
    print(fieldexpensesdictx)
    print(limeapplicationexpensedictx)
    print(year2021expensedictx)
    print(fieldincomedictx)
    print(fsaincomedictx)
    """
    # setting up .csv
    currentPath = os.getcwd()
    csv_file = csvFileName

    # expense dictionary entries
    try:
        with open(csv_file, 'w', newline='') as csvfile:
            #Expenses
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in countyexpensedictx:
                writer.writerow(data)
            for data in propaneexpensedictx:
                writer.writerow(data)
            for data in cauvexpensedictx:
                writer.writerow(data)
            for data in germanmutualexpensedictx:
                writer.writerow(data)
            for data in miscexpensesdictx:
                writer.writerow(data)
            for data in fieldexpensesdictx:
                writer.writerow(data)
            for data in limeapplicationexpensedictx:
                writer.writerow(data)
            for data in year2025expensedictx:
                writer.writerow(data)        
            #write the total for this expense
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_expense_total)
            writer.writeheader()
            
            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()

            #Incomes
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            #writer.writeheader()
            for data in fieldincomedictx:
                writer.writerow(data)
            for data in fsaincomedictx:
                writer.writerow(data)
            for data in miscincomedictx:
                writer.writerow(data)
            #write the total for this incomes
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_income_total)
            writer.writeheader()

            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()

            #The overall total
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_grand_total)
            writer.writeheader()

            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()
            writer.writerow({'':''}) # nothing

            #place totals from income expense goupings
            writer = csv.DictWriter(csvfile,csv_column_grouped_expense_income)
            writer.writeheader()
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Property taxes Expenses' ,'Total': countyexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Propane Expenses' ,'Total': propaneexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Cauv Expenses' ,'Total': cauvexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total German Mutual Insurance' ,'Total': germanmutualexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Miscellaneous Expenses' ,'Total':  miscexpensesdictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Field Expenses' ,'Total':  fieldexpensesdictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Field 5 Year Lime Expenses' ,'Total':  limeapplicationexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Year 2025 Field Expenses' ,'Total':  year2025expensedictxval})
            
            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()
            writer.writerow({'':''}) # nothing
            
            # Just Farm Only income, expense and grand total
            writer = csv.DictWriter(csvfile, csv_columns_farm_only)
            writer.writeheader()
                        
            #The farm expense overall total
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_expense_total_farm_only)
            writer.writeheader()
            
            #The farm income overall total
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_income_total_farm_only)
            writer.writeheader()
            
            #The famm income - expense grand total
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_grand_total_farm_only)
            writer.writeheader()
            
    except:
        print("Error opening .csv file")
    
# Main program - Supports command line arguments if needed "
if __name__ == "__main__":

    # Call calculations
    expensecalc()
