# Name:      farm2020.py
#
# Purpose:  This python script outputs expenses, incomes from the farm  
#
# Author:    Thomas Rode
#
# Options:  None
#
# Note:
#
# Python Version: 3.7.4
#          
# Reference: https://www.python.org/ (open source)
#


import csv
import os

csvFileName = "farm2023.csv"

#############################################################################################################
#                                        EXPENSES
#  Note: All numerical entries here. If you need to add or replace expense grouping i.e "countyexpensedictx"
#        please search for this dictonary variable in script accordingly
# 
#############################################################################################################
countyexpensedictx = [
    {'Date': '1/21/2023','Income/Expense': 'Expense','Description':'putnamCountyTreasure', 'Amount'  : 1570.60},
    {'Date': '7/18/2023','Income/Expense': 'Expense','Description':'putnamCountyTreasure paid from main account', 'Amount'  : 1558.30},
    ]
propaneexpensedictx = [
    {'Date': '2/09/2023','Income/Expense': 'Expense','Description':'fortJenningsPropane1',  'Amount' : 560.56},
    {'Date': '7/26/2023','Income/Expense': 'Expense','Description':'fortJenningsPropane2 233 gallons@1055/gal',  'Amount' : 386.52},
    {'Date': '10/25/2023','Income/Expense': 'Expense','Description':'fortJenningsPropane3',  'Amount' : 280.08},
    ]
cauvexpensedictx = [
    {'Date': '1/1/2022','Income/Expense': 'Expense','Description':'cauvputnam',          'Amount' :  0.00},
    ]
germanmutualexpensedictx = [
    {'Date': '1/21/2023','Income/Expense': 'Expense','Description':'germanmutualfirst',  'Amount' :  881.50},
    {'Date': '7/26/2023','Income/Expense': 'Expense','Description':'germanmutualsecond',  'Amount' :  881.50},
    ]
miscexpensesdictx = [
    {'Date': '12/27/2022','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 200.00},
    {'Date': '1/08/2023','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 200.00},
    {'Date': '1/22/2023','Income/Expense': 'Expense','Description':'Cash Withdraw church', 'Amount' : 20.00},
    {'Date': '1/25/2023','Income/Expense': 'Expense','Description':'Cash Withraw',    'Amount' : 50.00},
    {'Date': '2/16/2023','Income/Expense': 'Expense','Description':'Basement Doctor',    'Amount' : 120.00},
    {'Date': '3/21/2023','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 300.00},
    {'Date': '3/31/2023','Income/Expense': 'Expense','Description':'Cash Withdraw Ethan Page',    'Amount' : 100.00},
    {'Date': '6/04/2023','Income/Expense': 'Expense','Description':'Cash Withdraw Megan Ratkewicz',    'Amount' : 50.00},
    {'Date': '6/08/2023','Income/Expense': 'Expense','Description':'Peacock water',    'Amount' : 120.00},
    {'Date': '8/15/2023','Income/Expense': 'Expense','Description':'Cash Withraw Lavish',    'Amount' : 255.00},
    {'Date': '8/20/2023','Income/Expense': 'Expense','Description':'AEP electric',    'Amount' : 403.71},
    {'Date': '9/15/2023','Income/Expense': 'Expense','Description':'AEP electric',    'Amount' : 341.75},
    {'Date': '11/14/2023','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 500.00},
    {'Date': '11/11/2023','Income/Expense': 'Expense','Description':'Welder',    'Amount' : 100.00},
    ]
fieldexpensesdictx = [
    {'Date': '7/18/2023','Income/Expense': 'Expense','Description':'PopCorn Spray20acres paid from main account', 'Amount' : 515.83},
    {'Date': '8/15/2023','Income/Expense': 'Expense','Description':'Double crop soybean seed 10Acres',            'Amount' :  1668.44},
    {'Date': '9/12/2023','Income/Expense': 'Expense','Description':'Double crop Spray soybeans 10 acres',         'Amount' :  327.96},
    {'Date': '12/29/2023','Income/Expense': 'Expense','Description':'PopCorn seed 20 acres',                      'Amount' :  1100.50},
    ]

limeapplicationexpensedictx = [
    {'Date': '1/1/2022','Income/Expense': 'Expense','Description':'limeforsoil',                  'Amount' : 0},
    ]

year2024expensedictx = [
    {'Date': '12/11/2023','Income/Expense': 'Expense','Description':'fertilizer for popcorn for 10 acres for 2024 season',  'Amount' : 1935.24},
    {'Date': '12/11/2023','Income/Expense': 'Expense','Description':'soybean spray for 20 acres for 2024 season',  'Amount' : 450.1},
    {'Date': '12/28/2023','Income/Expense': 'Expense','Description':'soybean seed  for 20 acres for 2024 season',  'Amount' : 1083.45},
    ]
 
#############################################################################################################
#                                        INCOME
#  Note: All numerical entries here. If you need to add or replace expense grouping i.e "fieldincomedictx"
#        please search for this dictonary variable in script accordingly
# 
#############################################################################################################
fieldincomedictx = [
    {'Date': '7/21/2023','Income/Expense': 'Income','Description':'wheat 10acres, 625 Bu @$6.25 Bu/Ac', 'Amount' :3886.58},
    {'Date': '7/26/2023','Income/Expense': 'Income','Description':'Straw 10 acres 28.17 Bale, 12.25 tons, $50/ton J.Bockey',  'Amount' :612.5},
    {'Date': '11/17/2023','Income/Expense': 'Income','Description':'Soybeans double crop 10acres, 29.5Bu/AC Bu (147Bu) @$13.31 Bu/Ac Bunge grain ', 'Amount' :1955.77},
    {'Date': '12/29/2023','Income/Expense': 'Income','Description':'PopCorn 20 acres, 6061Lbs/Ac, $470/Ton ', 'Amount' :14685.77},
    ]

fsaincomedictx = [
    {'Date': '1/1/2021','Income/Expense': 'Income','Description':'fsaincom cfap2','Amount' :0},
    ]
########################################## END ################################################################

def WriteDictToCSV(csv_file,csv_columns,dict_data):
    try:
        with open(csv_file, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in dict_data:
                writer.writerow(data)
    except: #IOError as (errno, strerror):
            print("Error")#("I/O error({0}): {1}".format(errno, strerror))    
    return 
    
def expensecalc():
    
    #sum up individual dictonary's

    # Column headers
    csv_columns                      = ['Date','Income/Expense','Description', 'Amount']
    csv_column_spacer                = ['','','']
    csv_column_expense_total          = ['','','Expenses Total']
    csv_column_income_total          = ['','','Income Total']
    csv_column_grand_total            = ['','','Grand Total']
    csv_column_grouped_expense_income = ['Income/Expense', 'Grouping Description','Total']

    # adding up expenses and adding to .csv
    countyexpensedictxval          = sum(item['Amount'] for item in countyexpensedictx)    
    propaneexpensedictxval        = sum(item['Amount'] for item in propaneexpensedictx)
    cauvexpensedictxval            = sum(item['Amount'] for item in cauvexpensedictx)
    germanmutualexpensedictxval    = sum(item['Amount'] for item in germanmutualexpensedictx)
    miscexpensesdictxval          = sum(item['Amount'] for item in miscexpensesdictx)
    fieldexpensesdictxval          = sum(item['Amount'] for item in fieldexpensesdictx)
    limeapplicationexpensedictxval = sum(item['Amount'] for item in limeapplicationexpensedictx)
    year2024expensedictxval        = sum(item['Amount'] for item in year2024expensedictx)

    # cumulative expenses total
    csv_column_expense_total.append(countyexpensedictxval + propaneexpensedictxval + cauvexpensedictxval + germanmutualexpensedictxval + miscexpensesdictxval + fieldexpensesdictxval + limeapplicationexpensedictxval + year2024expensedictxval)
    allexpenses = (countyexpensedictxval + propaneexpensedictxval + cauvexpensedictxval + germanmutualexpensedictxval + miscexpensesdictxval + fieldexpensesdictxval + limeapplicationexpensedictxval + year2024expensedictxval)

    # adding up income and adding to .csv
    fieldincomedictxval = sum(item['Amount'] for item in fieldincomedictx)
    fsaincomedictxval = sum(item['Amount'] for item in fsaincomedictx)

    # cumulative income total
    csv_column_income_total.append(fieldincomedictxval + fsaincomedictxval)
    allincomes = (fieldincomedictxval + fsaincomedictxval)

    # Overall total income - expenses
    csv_column_grand_total.append(allincomes - allexpenses)

    # print out results
    print("##############  EXPENSES ################")
    print("countyexpensedict sum ->        ", round(countyexpensedictxval, 2))
    print("propaneexpensedict sum ->        ", propaneexpensedictxval)
    print("cauvexpensedict sum ->          ", cauvexpensedictxval)
    print("germanmutualexpensedict sum ->  ", germanmutualexpensedictxval)
    print("miscexpensesdict sum ->          ", miscexpensesdictxval)
    print("fieldexpensesdict sum ->        ", round(fieldexpensesdictxval, 2))
    print("limeapplicationexpensedict sum ->", limeapplicationexpensedictxval)
    print("year2024expensedict sum ->      ", year2024expensedictxval)
    print("")
    print("Expense Total in dollars        $",round(allexpenses,2))
    print("")
    print("##############  INCOME ##################")
    print("fieldincomedict sum ->          ", fieldincomedictxval)
    print("fsaincomedict sum ->            ", fsaincomedictxval)
    print("")
    print("Income Total in dollars        $",round(allincomes,2))
    print("")
    print("##############  INCOME MINUS EXPENSES ####")
    print("Net result in dollars          $",round(allincomes - allexpenses,2))
    print("")
    print("##############  ITEMS ##################")
    print("")
    """
    print(countyexpensedictx)
    print(propaneexpensedictx)
    print(cauvexpensedictx)
    print(germanmutualexpensedictx)
    print(miscexpensesdictx)
    print(fieldexpensesdictx)
    print(limeapplicationexpensedictx)
    print(year2021expensedictx)
    print(fieldincomedictx)
    print(fsaincomedictx)
    """
    # setting up .csv
    currentPath = os.getcwd()
    csv_file = csvFileName

    # expense dictionary entries
    try:
        with open(csv_file, 'w', newline='') as csvfile:
            #Expenses
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in countyexpensedictx:
                writer.writerow(data)
            for data in propaneexpensedictx:
                writer.writerow(data)
            for data in cauvexpensedictx:
                writer.writerow(data)
            for data in germanmutualexpensedictx:
                writer.writerow(data)
            for data in miscexpensesdictx:
                writer.writerow(data)
            for data in fieldexpensesdictx:
                writer.writerow(data)
            for data in limeapplicationexpensedictx:
                writer.writerow(data)
            for data in year2024expensedictx:
                writer.writerow(data)        
            #write the total for this expense
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_expense_total)
            writer.writeheader()
            
            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()

            #Incomes
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            #writer.writeheader()
            for data in fieldincomedictx:
                writer.writerow(data)
            for data in fsaincomedictx:
                writer.writerow(data)
            #write the total for this incomes
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_income_total)
            writer.writeheader()

            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()

            #The overall total
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_grand_total)
            writer.writeheader()

            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()
            writer.writerow({'':''}) # nothing

            #place totals from income expense goupings
            writer = csv.DictWriter(csvfile,csv_column_grouped_expense_income)
            writer.writeheader()
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Property taxes Expenses' ,'Total': countyexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Propane Expenses' ,'Total': propaneexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Cauv Expenses' ,'Total': cauvexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total German Mutual Insurance' ,'Total': germanmutualexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Miscellaneous Expenses' ,'Total':  miscexpensesdictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Field Expenses' ,'Total':  fieldexpensesdictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Field 5 Year Lime Expenses' ,'Total':  limeapplicationexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Year 2024 Field Expenses' ,'Total':  year2024expensedictxval})
    except:
        print("Error opening .csv file")
    
# Main program - Supports command line arguments if needed "
if __name__ == "__main__":

    # Call calculations
    expensecalc()
