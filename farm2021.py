# Name:      farm2020.py
#
# Purpose:  This python script outputs expenses, incomes from the farm  
#
# Author:    Thomas Rode
#
# Options:  None
#
# Note:
#
# Python Version: 3.7.4
#          
# Reference: https://www.python.org/ (open source)
#


import csv
import os

csvFileName = "farm2021.csv"

#############################################################################################################
#                                        EXPENSES
#  Note: All numerical entries here. If you need to add or replace expense grouping i.e "countyexpensedictx"
#        please search for this dictonary variable in script accordingly
# 
#############################################################################################################
countyexpensedictx = [
    {'Date': '1/19/2021','Income/Expense': 'Expense','Description':'putnamCountyTreasure', 'Amount'  : 1590.04},
    {'Date': '6/16/2021','Income/Expense': 'Expense','Description':'putnamCountyTreasure', 'Amount'  : 1565.32},
    ]
propaneexpensedictx = [
    {'Date': '1/17/2021','Income/Expense': 'Expense','Description':'fortJenningsPropane1 155gallons@$1.39/gal',  'Amount' : 232.03},
    {'Date': '2/23/2021','Income/Expense': 'Expense','Description':'fortJenningsPropane2',  'Amount' : 325.63},
    {'Date': '4/21/2021','Income/Expense': 'Expense','Description':'fortJenningsPropane3',  'Amount' : 433.86},
    {'Date': '12/1/2021','Income/Expense': 'Expense','Description':'fortJenningsPropane4 217gallons@$2.09/gal',  'Amount' : 489.16},
    ]
cauvexpensedictx = [
    {'Date': '1/1/2021','Income/Expense': 'Expense','Description':'cauvputnam',          'Amount' :  0.00},
    ]
germanmutualexpensedictx = [
    {'Date': '1/7/2021','Income/Expense': 'Expense','Description':'germanmutualfirst',  'Amount' :  671.71},
    {'Date': '1/19/2021','Income/Expense': 'Expense','Description':'germanmutualfirst Deck',  'Amount' :  18.00},
    {'Date': '7/12/2021','Income/Expense': 'Expense','Description':'germanmutualsecond',  'Amount' :  689.71},
    ]
miscexpensesdictx = [
    {'Date': '1/1/2021','Income/Expense': 'Expense','Description':'No misc expenses',    'Amount' : 0.00},
    ]
fieldexpensesdictx = [
    {'Date': '4/14/2021','Income/Expense': 'Expense','Description':'Soybeanseed20acres',                      'Amount' : 683.12},
    {'Date': '5/02/2021','Income/Expense': 'Expense','Description':'soybeanspray20acres$489.19cornspray10acres$543.14',     'Amount' :  1032.33},
    {'Date': '7/10/2021','Income/Expense': 'Expense','Description':'soybeanspray20acres$350.78cornspray10acres$194.26',  'Amount' :  545.04},
    ]

limeapplicationexpensedictx = [
    {'Date': '1/1/2021','Income/Expense': 'Expense','Description':'limeforsoil',                  'Amount' : 0},
    ]

year2022expensedictx = [
    {'Date': '8/18/2021','Income/Expense': 'Expense','Description':'wheatseed for 20acresfor2022 season 216.10 out of our checking acct',  'Amount' : 500.00},
    {'Date': '10/11/2021','Income/Expense': 'Expense','Description':'wheatspray and fertilizer for 20acresfor2022 season',  'Amount' : 1693.59},
    {'Date': '12/13/2021','Income/Expense': 'Expense','Description':'soybeanspray for 10acresfor2022 season',  'Amount' : 155.51}, 
    ]

#############################################################################################################
#                                        INCOME
#  Note: All numerical entries here. If you need to add or replace expense grouping i.e "fieldincomedictx"
#        please search for this dictonary variable in script accordingly
# 
#############################################################################################################
fieldincomedictx = [
    {'Date': '9/28/2021','Income/Expense': 'Income','Description':'soybeans20acres, 711Bu@$12.37/Bu', 'Amount' :8759.01},
    {'Date': '12/31/2021','Income/Expense': 'Income','Description':'popcorn10acres, yield/acre 5907lbs, $360/ton, $2899.1 seed cost($483.28@16.67%)',  'Amount' :4834.48},
    ]

fsaincomedictx = [
    {'Date': '4/7/2021','Income/Expense': 'Income','Description':'fsaincom cfap2','Amount' :312.40},
    {'Date': '10/12/2021','Income/Expense': 'Income','Description':'fsaincome pricelosscoverage 2020','Amount' :68},
    ]
########################################## END ################################################################

def WriteDictToCSV(csv_file,csv_columns,dict_data):
    try:
        with open(csv_file, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in dict_data:
                writer.writerow(data)
    except: #IOError as (errno, strerror):
            print("Error")#("I/O error({0}): {1}".format(errno, strerror))    
    return 
    
def expensecalc():
    
    #sum up individual dictonary's

    # Column headers
    csv_columns                      = ['Date','Income/Expense','Description', 'Amount']
    csv_column_spacer                = ['','','']
    csv_column_expense_total          = ['','','Expenses Total']
    csv_column_income_total          = ['','','Income Total']
    csv_column_grand_total            = ['','','Grand Total']
    csv_column_grouped_expense_income = ['Income/Expense', 'Grouping Description','Total']

    # adding up expenses and adding to .csv
    countyexpensedictxval          = sum(item['Amount'] for item in countyexpensedictx)    
    propaneexpensedictxval        = sum(item['Amount'] for item in propaneexpensedictx)
    cauvexpensedictxval            = sum(item['Amount'] for item in cauvexpensedictx)
    germanmutualexpensedictxval    = sum(item['Amount'] for item in germanmutualexpensedictx)
    miscexpensesdictxval          = sum(item['Amount'] for item in miscexpensesdictx)
    fieldexpensesdictxval          = sum(item['Amount'] for item in fieldexpensesdictx)
    limeapplicationexpensedictxval = sum(item['Amount'] for item in limeapplicationexpensedictx)
    year2022expensedictxval        = sum(item['Amount'] for item in year2022expensedictx)

    # cumulative expenses total
    csv_column_expense_total.append(countyexpensedictxval + propaneexpensedictxval + cauvexpensedictxval + germanmutualexpensedictxval + miscexpensesdictxval + fieldexpensesdictxval + limeapplicationexpensedictxval + year2022expensedictxval)
    allexpenses = (countyexpensedictxval + propaneexpensedictxval + cauvexpensedictxval + germanmutualexpensedictxval + miscexpensesdictxval + fieldexpensesdictxval + limeapplicationexpensedictxval + year2022expensedictxval)

    # adding up income and adding to .csv
    fieldincomedictxval = sum(item['Amount'] for item in fieldincomedictx)
    fsaincomedictxval = sum(item['Amount'] for item in fsaincomedictx)

    # cumulative income total
    csv_column_income_total.append(fieldincomedictxval + fsaincomedictxval)
    allincomes = (fieldincomedictxval + fsaincomedictxval)

    # Overall total income - expenses
    csv_column_grand_total.append(allincomes - allexpenses)

    # print out results
    print("##############  EXPENSES ################")
    print("countyexpensedict sum ->        ", round(countyexpensedictxval, 2))
    print("propaneexpensedict sum ->        ", propaneexpensedictxval)
    print("cauvexpensedict sum ->          ", cauvexpensedictxval)
    print("germanmutualexpensedict sum ->  ", germanmutualexpensedictxval)
    print("miscexpensesdict sum ->          ", miscexpensesdictxval)
    print("fieldexpensesdict sum ->        ", round(fieldexpensesdictxval, 2))
    print("limeapplicationexpensedict sum ->", limeapplicationexpensedictxval)
    print("year2022expensedict sum ->      ", year2022expensedictxval)
    print("")
    print("Expense Total in dollars        $",round(allexpenses,2))
    print("")
    print("##############  INCOME ##################")
    print("fieldincomedict sum ->          ", fieldincomedictxval)
    print("fsaincomedict sum ->            ", fsaincomedictxval)
    print("")
    print("Income Total in dollars        $",round(allincomes,2))
    print("")
    print("##############  INCOME MINUS EXPENSES ####")
    print("Net result in dollars          $",round(allincomes - allexpenses,2))
    print("")
    print("##############  ITEMS ##################")
    print("")
    """
    print(countyexpensedictx)
    print(propaneexpensedictx)
    print(cauvexpensedictx)
    print(germanmutualexpensedictx)
    print(miscexpensesdictx)
    print(fieldexpensesdictx)
    print(limeapplicationexpensedictx)
    print(year2021expensedictx)
    print(fieldincomedictx)
    print(fsaincomedictx)
    """
    # setting up .csv
    currentPath = os.getcwd()
    csv_file = csvFileName

    # expense dictionary entries
    try:
        with open(csv_file, 'w', newline='') as csvfile:
            #Expenses
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in countyexpensedictx:
                writer.writerow(data)
            for data in propaneexpensedictx:
                writer.writerow(data)
            for data in cauvexpensedictx:
                writer.writerow(data)
            for data in germanmutualexpensedictx:
                writer.writerow(data)
            for data in miscexpensesdictx:
                writer.writerow(data)
            for data in fieldexpensesdictx:
                writer.writerow(data)
            for data in limeapplicationexpensedictx:
                writer.writerow(data)
            for data in year2022expensedictx:
                writer.writerow(data)        
            #write the total for this expense
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_expense_total)
            writer.writeheader()
            
            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()

            #Incomes
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            #writer.writeheader()
            for data in fieldincomedictx:
                writer.writerow(data)
            for data in fsaincomedictx:
                writer.writerow(data)
            #write the total for this incomes
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_income_total)
            writer.writeheader()

            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()

            #The overall total
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_grand_total)
            writer.writeheader()

            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()
            writer.writerow({'':''}) # nothing

            #place totals from income expense goupings
            writer = csv.DictWriter(csvfile,csv_column_grouped_expense_income)
            writer.writeheader()
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Property taxes Expenses' ,'Total': countyexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Propane Expenses' ,'Total': propaneexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Cauv Expenses' ,'Total': cauvexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total German Mutual Insurance' ,'Total': germanmutualexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Miscellaneous Expenses' ,'Total':  miscexpensesdictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Field Expenses' ,'Total':  fieldexpensesdictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Field 5 Year Lime Expenses' ,'Total':  limeapplicationexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Year 2022 Field Expenses' ,'Total':  year2022expensedictxval})
    except:
        print("Error opening .csv file")
    
# Main program - Supports command line arguments if needed "
if __name__ == "__main__":

    # Call calculations
    expensecalc()
