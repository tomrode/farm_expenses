# Name:      farm2020.py
#
# Purpose:  This python script outputs expenses, incomes from the farm  
#
# Author:    Thomas Rode
#
# Options:  None
#
# Note:
#
# Python Version: 3.7.4
#          
# Reference: https://www.python.org/ (open source)
#


import csv
import os

csvFileName = "farm2020.csv"

#############################################################################################################
#                                        EXPENSES
#  Note: All numerical entries here. If you need to add or replace expense grouping i.e "countyexpensedictx"
#        please search for this dictonary variable in script accordingly
# 
#############################################################################################################
countyexpensedictx = [
    {'Date': '1/28/2020','Income/Expense': 'Expense','Description':'putnamCountyTreasure', 'Amount'  : 1538.06},
    {'Date': '7/10/2020','Income/Expense': 'Expense','Description':'putnamCountyTreasure', 'Amount'  : 1526.78},
    ]
propaneexpensedictx = [
    {'Date': '1/14/2020','Income/Expense': 'Expense','Description':'fortJenningsPropane1',  'Amount' :  387.99},
    {'Date': '2/21/2020','Income/Expense': 'Expense','Description':'fortJenningsPropane2',  'Amount' :  370.82},
    ]
cauvexpensedictx = [
    {'Date': '1/20/2020','Income/Expense': 'Expense','Description':'cauvputnam',          'Amount' :  25.00},
    ]
germanmutualexpensedictx = [
    {'Date': '1/8/2020','Income/Expense': 'Expense','Description':'germanmutualfirst',  'Amount' :  670.71},
    {'Date': '7/22/2020','Income/Expense': 'Expense','Description':'germanmutualsecond',  'Amount' :  670.71},
    ]
miscexpensesdictx = [
    {'Date': '1/1/2020','Income/Expense': 'Expense','Description':'Drone perhaps here Best buy $1099.94',    'Amount' :  1099.94},
    ]
fieldexpensesdictx = [
    {'Date': '5/9/2020','Income/Expense': 'Expense','Description':'wheatspray10acres',                      'Amount' :  247.56},
    {'Date': '7/10/2020','Income/Expense': 'Expense','Description':'popcornspray20acres',                   'Amount' :  336.73},
    {'Date': '8/10/2020','Income/Expense': 'Expense','Description':'grid samples and soybean seed10acres',  'Amount' :  244.34},
    {'Date': '9/9/2020','Income/Expense': 'Expense','Description':'soybeanspray10acres',                    'Amount' :  141.87},
    {'Date': '6/10/2020','Income/Expense': 'Expense','Description':'Paid from Reg acct, 20acrecorn fert $587, corn spray $237, 10acre wht spray  ', 'Amount' :  965.48},
    ]

limeapplicationexpensedictx = [
    {'Date': '1/1/2020','Income/Expense': 'Expense','Description':'limeforsoil',                  'Amount' : 0},
    ]

year2021expensedictx = [
    {'Date': '12/9/2020','Income/Expense': 'Expense','Description':'spray&fert10acres spray 20acres2021',  'Amount' : 1159.86},
    {'Date': '12/30/2020','Income/Expense': 'Expense','Description':'soybeanseed 6% discount 20acres2021',  'Amount' : 341.89}, 
    ]

#############################################################################################################
#                                        INCOME
#  Note: All numerical entries here. If you need to add or replace expense grouping i.e "fieldincomedictx"
#        please search for this dictonary variable in script accordingly
# 
#############################################################################################################
fieldincomedictx = [
    {'Date': '7/10/2020','Income/Expense': 'Income','Description':'wheat10acres, 477.4Bu@$4.87/Bu', 'Amount' :2301.58},
    {'Date': '7/22/2020','Income/Expense': 'Income','Description':'wheatstraw10acres, 20.83 bales', 'Amount' :531.00}, 
    {'Date': '11/13/2020','Income/Expense': 'Income','Description':'doublecropsoybeanseed10acres 202Bu@$11.07/Bu 40.46bu/acre',  'Amount' :2228.09},
    {'Date': '12/29/2020','Income/Expense': 'Income','Description':'popcorn20-21acres, $5224/acre, $300/ton ',  'Amount' :7337.29},
    ]

fsaincomedictx = [
    {'Date': '1//2020','Income/Expense': 'Income','Description':'fsaincomeoctober pricelosscoverage 2019','Amount' :210.60},
    {'Date': '10/9/2020','Income/Expense': 'Income','Description':'fsaincomeoctober pricelosscoverage 2019','Amount' :116},
    {'Date': '10/28/2020','Income/Expense': 'Income','Description':'fsaincomeoctober cfap2',            'Amount' :513.41},
    ]
########################################## END ################################################################

def WriteDictToCSV(csv_file,csv_columns,dict_data):
    try:
        with open(csv_file, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in dict_data:
                writer.writerow(data)
    except: #IOError as (errno, strerror):
            print("Error")#("I/O error({0}): {1}".format(errno, strerror))    
    return 
    
def expensecalc():
    
    #sum up individual dictonary's

    # Column headers
    csv_columns                      = ['Date','Income/Expense','Description', 'Amount']
    csv_column_spacer                = ['','','']
    csv_column_expense_total          = ['','','Expenses Total']
    csv_column_income_total          = ['','','Income Total']
    csv_column_grand_total            = ['','','Grand Total']
    csv_column_grouped_expense_income = ['Income/Expense', 'Grouping Description','Total']

    # adding up expenses and adding to .csv
    countyexpensedictxval          = sum(item['Amount'] for item in countyexpensedictx)    
    propaneexpensedictxval        = sum(item['Amount'] for item in propaneexpensedictx)
    cauvexpensedictxval            = sum(item['Amount'] for item in cauvexpensedictx)
    germanmutualexpensedictxval    = sum(item['Amount'] for item in germanmutualexpensedictx)
    miscexpensesdictxval          = sum(item['Amount'] for item in miscexpensesdictx)
    fieldexpensesdictxval          = sum(item['Amount'] for item in fieldexpensesdictx)
    limeapplicationexpensedictxval = sum(item['Amount'] for item in limeapplicationexpensedictx)
    year2021expensedictxval        = sum(item['Amount'] for item in year2021expensedictx)

    # cumulative expenses total
    csv_column_expense_total.append(countyexpensedictxval + propaneexpensedictxval + cauvexpensedictxval + germanmutualexpensedictxval + miscexpensesdictxval + fieldexpensesdictxval + limeapplicationexpensedictxval + year2021expensedictxval)
    allexpenses = (countyexpensedictxval + propaneexpensedictxval + cauvexpensedictxval + germanmutualexpensedictxval + miscexpensesdictxval + fieldexpensesdictxval + limeapplicationexpensedictxval + year2021expensedictxval)

    # adding up income and adding to .csv
    fieldincomedictxval = sum(item['Amount'] for item in fieldincomedictx)
    fsaincomedictxval = sum(item['Amount'] for item in fsaincomedictx)

    # cumulative income total
    csv_column_income_total.append(fieldincomedictxval + fsaincomedictxval)
    allincomes = (fieldincomedictxval + fsaincomedictxval)

    # Overall total income - expenses
    csv_column_grand_total.append(allincomes - allexpenses)

    # print out results
    print("##############  EXPENSES ################")
    print("countyexpensedict sum ->        ", round(countyexpensedictxval, 2))
    print("propaneexpensedict sum ->        ", propaneexpensedictxval)
    print("cauvexpensedict sum ->          ", cauvexpensedictxval)
    print("germanmutualexpensedict sum ->  ", germanmutualexpensedictxval)
    print("miscexpensesdict sum ->          ", miscexpensesdictxval)
    print("fieldexpensesdict sum ->        ", round(fieldexpensesdictxval, 2))
    print("limeapplicationexpensedict sum ->", limeapplicationexpensedictxval)
    print("year2021expensedict sum ->      ", year2021expensedictxval)
    print("")
    print("Expense Total in dollars        $",round(allexpenses,2))
    print("")
    print("##############  INCOME ##################")
    print("fieldincomedict sum ->          ", fieldincomedictxval)
    print("fsaincomedict sum ->            ", fsaincomedictxval)
    print("")
    print("Income Total in dollars        $",round(allincomes,2))
    print("")
    print("##############  INCOME MINUS EXPENSES ####")
    print("Net result in dollars          $",round(allincomes - allexpenses,2))
    print("")
    print("##############  ITEMS ##################")
    print("")
    """
    print(countyexpensedictx)
    print(propaneexpensedictx)
    print(cauvexpensedictx)
    print(germanmutualexpensedictx)
    print(miscexpensesdictx)
    print(fieldexpensesdictx)
    print(limeapplicationexpensedictx)
    print(year2021expensedictx)
    print(fieldincomedictx)
    print(fsaincomedictx)
    """
    # setting up .csv
    currentPath = os.getcwd()
    csv_file = csvFileName

    # expense dictionary entries
    try:
        with open(csv_file, 'w', newline='') as csvfile:
            #Expenses
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in countyexpensedictx:
                writer.writerow(data)
            for data in propaneexpensedictx:
                writer.writerow(data)
            for data in cauvexpensedictx:
                writer.writerow(data)
            for data in germanmutualexpensedictx:
                writer.writerow(data)
            for data in miscexpensesdictx:
                writer.writerow(data)
            for data in fieldexpensesdictx:
                writer.writerow(data)
            for data in limeapplicationexpensedictx:
                writer.writerow(data)
            for data in year2021expensedictx:
                writer.writerow(data)        
            #write the total for this expense
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_expense_total)
            writer.writeheader()
            
            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()

            #Incomes
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            #writer.writeheader()
            for data in fieldincomedictx:
                writer.writerow(data)
            for data in fsaincomedictx:
                writer.writerow(data)
            #write the total for this incomes
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_income_total)
            writer.writeheader()

            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()

            #The overall total
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_grand_total)
            writer.writeheader()

            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()
            writer.writerow({'':''}) # nothing

            #place totals from income expense goupings
            writer = csv.DictWriter(csvfile,csv_column_grouped_expense_income)
            writer.writeheader()
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Property taxes Expenses' ,'Total': countyexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Propane Expenses' ,'Total': propaneexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Cauv Expenses' ,'Total': cauvexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total German Mutual Insurance' ,'Total': germanmutualexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Miscellaneous Expenses' ,'Total':  miscexpensesdictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Field Expenses' ,'Total':  fieldexpensesdictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Field 5 Year Lime Expenses' ,'Total':  limeapplicationexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Year 2020 Field Expenses' ,'Total':  year2021expensedictxval})
    except:
        print("Error opening .csv file")
    
# Main program - Supports command line arguments if needed "
if __name__ == "__main__":

    # Call calculations
    expensecalc()
