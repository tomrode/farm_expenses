# Name:      farm2020.py
#
# Purpose:  This python script outputs expenses, incomes from the farm  
#
# Author:    Thomas Rode
#
# Options:  None
#
# Note:
#
# Python Version: 3.7.4
#          
# Reference: https://www.python.org/ (open source)
#


import csv
import os

csvFileName = "farm2022.csv"

#############################################################################################################
#                                        EXPENSES
#  Note: All numerical entries here. If you need to add or replace expense grouping i.e "countyexpensedictx"
#        please search for this dictonary variable in script accordingly
# 
#############################################################################################################
countyexpensedictx = [
    {'Date': '1/20/2022','Income/Expense': 'Expense','Description':'putnamCountyTreasure', 'Amount'  : 1577.21},
    {'Date': '7/07/2022','Income/Expense': 'Expense','Description':'putnamCountyTreasure', 'Amount'  : 1559.27},
    ]
propaneexpensedictx = [
    {'Date': '1/22/2022','Income/Expense': 'Expense','Description':'fortJenningsPropane1',  'Amount' : 364.07},
    {'Date': '3/14/2022','Income/Expense': 'Expense','Description':'fortJenningsPropane2',  'Amount' : 489.53},
    {'Date': '7/12/2022','Income/Expense': 'Expense','Description':'fortJenningsPropane3',  'Amount' : 268.64},
    {'Date': '12/11/2022','Income/Expense': 'Expense','Description':'fortJenningsPropane4 156 gallons@$2.04/gal',  'Amount' : 342.68},
    ]
cauvexpensedictx = [
    {'Date': '1/1/2022','Income/Expense': 'Expense','Description':'cauvputnam',          'Amount' :  0.00},
    ]
germanmutualexpensedictx = [
    {'Date': '1/22/2022','Income/Expense': 'Expense','Description':'germanmutualfirst',  'Amount' :  789.22},
    {'Date': '7/27/2022','Income/Expense': 'Expense','Description':'germanmutualsecond',  'Amount' :  798.22},
    ]
miscexpensesdictx = [
    {'Date': '1/3/2022','Income/Expense': 'Expense','Description':'Bank statement adjustment',    'Amount' : 40.00},
    {'Date': '1/12/2022','Income/Expense': 'Expense','Description':'Peacock water Salt',    'Amount' : 101.51},
    {'Date': '1/22/2022','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 200.00},
    {'Date': '1/26/2022','Income/Expense': 'Expense','Description':'AEP electric',    'Amount' : 247.85},
    {'Date': '2/22/2022','Income/Expense': 'Expense','Description':'Steve Meyers Service mower',    'Amount' : 385.45},
    {'Date': '2/22/2022','Income/Expense': 'Expense','Description':'Basement Doctor',    'Amount' : 120.00},
    {'Date': '3/14/2022','Income/Expense': 'Expense','Description':'Dale Cavelage tree stump removal',    'Amount' : 100.00},
    {'Date': '4/08/2022','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 300.00},
    {'Date': '5/16/2022','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 400.00},
    {'Date': '6/07/2022','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 400.00},
    {'Date': '6/13/2022','Income/Expense': 'Expense','Description':'ACR-Dumpstser',    'Amount' : 250.00},
    {'Date': '7/08/2022','Income/Expense': 'Expense','Description':'Rock',    'Amount' : 308.68},
    {'Date': '7/19/2022','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 800.00},
    {'Date': '7/19/2022','Income/Expense': 'Expense','Description':'AEP electric',    'Amount' : 357.86},
    {'Date': '7/25/2022','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 500.00},
    {'Date': '8/01/2022','Income/Expense': 'Expense','Description':'Well pump Grothhaus',    'Amount' : 1931.66},
    {'Date': '8/01/2022','Income/Expense': 'Expense','Description':'Oliver Tractor purchase from Steve Warneke',    'Amount' : 2500.00},
    {'Date': '8/03/2022','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 500.00},
    {'Date': '8/10/2022','Income/Expense': 'Expense','Description':'Peacock water Salt',    'Amount' : 112.19},
    {'Date': '8/10/2022','Income/Expense': 'Expense','Description':'AEP electric',    'Amount' : 304.66},
    {'Date': '8/22/2022','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 500.00},
    {'Date': '11/22/2022','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 500.00},
    {'Date': '11/28/2022','Income/Expense': 'Expense','Description':'Cash Withdraw',    'Amount' : 200.00},
    {'Date': '12/14/2022','Income/Expense': 'Expense','Description':'AEP electric',    'Amount' : 215.40},
    ]
fieldexpensesdictx = [
    {'Date': '1/03/2022','Income/Expense': 'Expense','Description':'Soybeanseed10acres',                      'Amount' : 494.30},
    {'Date': '5/02/2022','Income/Expense': 'Expense','Description':'Fertilizer Wheat 20Acres',     'Amount' :  1668.44},
    {'Date': '7/10/2022','Income/Expense': 'Expense','Description':'soybeanspray 10 acres',  'Amount' :  421.38},
    {'Date': '7/11/2022','Income/Expense': 'Expense','Description':'Soybean fertilizer 10acres',     'Amount' : 263.70},
    {'Date': '8/08/2022','Income/Expense': 'Expense','Description':'Soybeanseed 20acres double crop','Amount' : 612.00},
    {'Date': '9/12/2022','Income/Expense': 'Expense','Description':'soybean spray 20 acres double crop',  'Amount' :  747.97},
    ]

limeapplicationexpensedictx = [
    {'Date': '1/1/2022','Income/Expense': 'Expense','Description':'limeforsoil',                  'Amount' : 0},
    ]

year2023expensedictx = [
    {'Date': '8/14/2022','Income/Expense': 'Expense','Description':'wheatseed for 10 acres for 2023 season',  'Amount' : 386.25},
    {'Date': '11/13/2022','Income/Expense': 'Expense','Description':'wheat fertilzer and spray for 10 acres for 2023 season',  'Amount' : 1157.72},
    {'Date': '12/11/2021','Income/Expense': 'Expense','Description':'popcorn fertilizer for 20 acres for 2023 season',  'Amount' : 2143.57},
    {'Date': '12/11/2021','Income/Expense': 'Expense','Description':'popcorn fertilizer (NH3) for 20 acres for 2023 season',  'Amount' : 2213.08}, 
    ]

#############################################################################################################
#                                        INCOME
#  Note: All numerical entries here. If you need to add or replace expense grouping i.e "fieldincomedictx"
#        please search for this dictonary variable in script accordingly
# 
#############################################################################################################
fieldincomedictx = [
    {'Date': '7/11/2021','Income/Expense': 'Income','Description':'wheat 20acres, 1025 Bu @$8.36-8.82/Bu', 'Amount' :8777.59},
    {'Date': '7/20/2021','Income/Expense': 'Income','Description':'Straw 20 acres 44.5 Bales J.Bockey',  'Amount' :1107.60},
    {'Date': '10/11/2021','Income/Expense': 'Income','Description':'Soybeans 10acres, 346.52 Bu @$13.57Bu/Ac Hempker grain ', 'Amount' :4677.78},
    {'Date': '11/14/2021','Income/Expense': 'Income','Description':'Soybeans 20acres, 486.66 Bu @$13.71Bu/Ac Hempker grain ', 'Amount' :6618.32},
    ]

fsaincomedictx = [
    {'Date': '1/1/2021','Income/Expense': 'Income','Description':'fsaincom cfap2','Amount' :0},
    ]
########################################## END ################################################################

def WriteDictToCSV(csv_file,csv_columns,dict_data):
    try:
        with open(csv_file, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in dict_data:
                writer.writerow(data)
    except: #IOError as (errno, strerror):
            print("Error")#("I/O error({0}): {1}".format(errno, strerror))    
    return 
    
def expensecalc():
    
    #sum up individual dictonary's

    # Column headers
    csv_columns                      = ['Date','Income/Expense','Description', 'Amount']
    csv_column_spacer                = ['','','']
    csv_column_expense_total          = ['','','Expenses Total']
    csv_column_income_total          = ['','','Income Total']
    csv_column_grand_total            = ['','','Grand Total']
    csv_column_grouped_expense_income = ['Income/Expense', 'Grouping Description','Total']

    # adding up expenses and adding to .csv
    countyexpensedictxval          = sum(item['Amount'] for item in countyexpensedictx)    
    propaneexpensedictxval        = sum(item['Amount'] for item in propaneexpensedictx)
    cauvexpensedictxval            = sum(item['Amount'] for item in cauvexpensedictx)
    germanmutualexpensedictxval    = sum(item['Amount'] for item in germanmutualexpensedictx)
    miscexpensesdictxval          = sum(item['Amount'] for item in miscexpensesdictx)
    fieldexpensesdictxval          = sum(item['Amount'] for item in fieldexpensesdictx)
    limeapplicationexpensedictxval = sum(item['Amount'] for item in limeapplicationexpensedictx)
    year2023expensedictxval        = sum(item['Amount'] for item in year2023expensedictx)

    # cumulative expenses total
    csv_column_expense_total.append(countyexpensedictxval + propaneexpensedictxval + cauvexpensedictxval + germanmutualexpensedictxval + miscexpensesdictxval + fieldexpensesdictxval + limeapplicationexpensedictxval + year2023expensedictxval)
    allexpenses = (countyexpensedictxval + propaneexpensedictxval + cauvexpensedictxval + germanmutualexpensedictxval + miscexpensesdictxval + fieldexpensesdictxval + limeapplicationexpensedictxval + year2023expensedictxval)

    # adding up income and adding to .csv
    fieldincomedictxval = sum(item['Amount'] for item in fieldincomedictx)
    fsaincomedictxval = sum(item['Amount'] for item in fsaincomedictx)

    # cumulative income total
    csv_column_income_total.append(fieldincomedictxval + fsaincomedictxval)
    allincomes = (fieldincomedictxval + fsaincomedictxval)

    # Overall total income - expenses
    csv_column_grand_total.append(allincomes - allexpenses)

    # print out results
    print("##############  EXPENSES ################")
    print("countyexpensedict sum ->        ", round(countyexpensedictxval, 2))
    print("propaneexpensedict sum ->        ", propaneexpensedictxval)
    print("cauvexpensedict sum ->          ", cauvexpensedictxval)
    print("germanmutualexpensedict sum ->  ", germanmutualexpensedictxval)
    print("miscexpensesdict sum ->          ", miscexpensesdictxval)
    print("fieldexpensesdict sum ->        ", round(fieldexpensesdictxval, 2))
    print("limeapplicationexpensedict sum ->", limeapplicationexpensedictxval)
    print("year2022expensedict sum ->      ", year2023expensedictxval)
    print("")
    print("Expense Total in dollars        $",round(allexpenses,2))
    print("")
    print("##############  INCOME ##################")
    print("fieldincomedict sum ->          ", fieldincomedictxval)
    print("fsaincomedict sum ->            ", fsaincomedictxval)
    print("")
    print("Income Total in dollars        $",round(allincomes,2))
    print("")
    print("##############  INCOME MINUS EXPENSES ####")
    print("Net result in dollars          $",round(allincomes - allexpenses,2))
    print("")
    print("##############  ITEMS ##################")
    print("")
    """
    print(countyexpensedictx)
    print(propaneexpensedictx)
    print(cauvexpensedictx)
    print(germanmutualexpensedictx)
    print(miscexpensesdictx)
    print(fieldexpensesdictx)
    print(limeapplicationexpensedictx)
    print(year2021expensedictx)
    print(fieldincomedictx)
    print(fsaincomedictx)
    """
    # setting up .csv
    currentPath = os.getcwd()
    csv_file = csvFileName

    # expense dictionary entries
    try:
        with open(csv_file, 'w', newline='') as csvfile:
            #Expenses
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in countyexpensedictx:
                writer.writerow(data)
            for data in propaneexpensedictx:
                writer.writerow(data)
            for data in cauvexpensedictx:
                writer.writerow(data)
            for data in germanmutualexpensedictx:
                writer.writerow(data)
            for data in miscexpensesdictx:
                writer.writerow(data)
            for data in fieldexpensesdictx:
                writer.writerow(data)
            for data in limeapplicationexpensedictx:
                writer.writerow(data)
            for data in year2023expensedictx:
                writer.writerow(data)        
            #write the total for this expense
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_expense_total)
            writer.writeheader()
            
            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()

            #Incomes
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            #writer.writeheader()
            for data in fieldincomedictx:
                writer.writerow(data)
            for data in fsaincomedictx:
                writer.writerow(data)
            #write the total for this incomes
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_income_total)
            writer.writeheader()

            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()

            #The overall total
            writer = csv.DictWriter(csvfile, fieldnames=csv_column_grand_total)
            writer.writeheader()

            #place a spacing
            writer = csv.DictWriter(csvfile,csv_column_spacer)
            writer.writeheader()
            writer.writerow({'':''}) # nothing

            #place totals from income expense goupings
            writer = csv.DictWriter(csvfile,csv_column_grouped_expense_income)
            writer.writeheader()
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Property taxes Expenses' ,'Total': countyexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Propane Expenses' ,'Total': propaneexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Cauv Expenses' ,'Total': cauvexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total German Mutual Insurance' ,'Total': germanmutualexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Miscellaneous Expenses' ,'Total':  miscexpensesdictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Field Expenses' ,'Total':  fieldexpensesdictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Field 5 Year Lime Expenses' ,'Total':  limeapplicationexpensedictxval})
            writer.writerow({'Income/Expense': 'Expense', 'Grouping Description': 'Total Year 2022 Field Expenses' ,'Total':  year2023expensedictxval})
    except:
        print("Error opening .csv file")
    
# Main program - Supports command line arguments if needed "
if __name__ == "__main__":

    # Call calculations
    expensecalc()
